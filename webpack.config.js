var webpack = require('webpack');
var fs = require('fs');
var path = require('path');

module.exports = {
  entry: [
    'webpack-dev-server/client?http://0.0.0.0:8080',
    'webpack/hot/only-dev-server',
    './src/main',
    'react-router'
  ],
  output: {
    path: 'build/',
    filename: 'bundle.js',
    publicPath: 'http://localhost:8080/build/'
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'react-hot!babel?optional[]=runtime&stage=0' },
      { test: /\.css$/, loader: 'style!css?localIdentName=[name]-[local]--[hash:base64:5]' },
      { test: /\.jpg|png$/, loader: 'url!limit=8192' }
    ]
  },
  resolve: {
    extensions: ['', '.js','.json']
  },
  plugins: [
    // new webpack.HotModuleReplacementPlugin(),
    // new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js'),
    new webpack.NoErrorsPlugin()
  ]
};
